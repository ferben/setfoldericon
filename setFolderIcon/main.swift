//
//  main.swift
//  setFolderIcon
//
//  Created by Frantisek Erben on 27.01.2017.
//  Copyright © 2017 Frantisek Erben (erben.fr@gmail.com). All rights reserved.
//

import Cocoa

let args = CommandLine.arguments

let helpStr = "Usage: setFolderIcon [file.icns|png|jpg|tiff] [folder]\n© 2017 František Erben (erben.fr@gmail.com)"

if args.count>1 && args.count<4 {
	let icnsFile     = URL.init(fileURLWithPath: args[1])
	let targetFolder = URL.init(fileURLWithPath: args[2])
	
	let icnsImage=NSImage.init(contentsOf: icnsFile)
	
	let ws = NSWorkspace.shared()
	let res = ws.setIcon(icnsImage, forFile: targetFolder.path, options: NSWorkspaceIconCreationOptions.excludeQuickDrawElementsIconCreationOption)
	if res {
		print("Icon was set successfully!")
		exit(0)
	}
	else {
		print("Something was wrong. Please check parameter's order\n\(helpStr)")
		exit(1)
	}
}
else {
	print(helpStr)
}



















exit(0)
